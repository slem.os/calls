Name:       calls
Version:    0.1.8
Release:    1%{?dist}
Summary:    A phone dialer and call handler.

License:        GPLv3+ and MIT
URL:            https://source.puri.sm/Librem5/calls
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/calls/-/archive/v%{version}/%{name}-v%{version}.tar.gz

Patch0:         0001-fix-audio-in-calls

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  cmake

BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(libhandy-0.0) >= 0.0.12
BuildRequires:  pkgconfig(gsound)
BuildRequires:  pkgconfig(libpeas-1.0)
BuildRequires:  pkgconfig(gom-1.0)
BuildRequires:  pkgconfig(libebook-contacts-1.2)
BuildRequires:  pkgconfig(folks)
BuildRequires:  pkgconfig(mm-glib)
BuildRequires:  appstream-glib 
BuildRequires:  vala
BuildRequires:  desktop-file-utils
BuildRequires:  xvfb-run
BuildRequires:  xauth
BuildRequires:  libappstream-glib8
BuildRequires:  feedbackd-devel

Requires: hicolor-icon-theme


%description
A phone dialer and call handler.


%prep
%setup -q -n %{name}-v%{version}
%patch -p0

%build
%meson
%meson_build


%install
%meson_install


%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/sm.puri.Calls.metainfo.xml

desktop-file-validate %{buildroot}/%{_datadir}/applications/sm.puri.Calls.desktop

LC_ALL=C.UTF-8 xvfb-run sh <<'SH'
%meson_test
SH


%files
%{_sysconfdir}/xdg/autostart/sm.puri.Calls.desktop
%{_bindir}/calls

%dir %{_libdir}/calls
%dir %{_libdir}/calls/plugins
%dir %{_libdir}/calls/plugins/mm
%dir %{_libdir}/calls/plugins/dummy

%{_libdir}/calls/plugins/mm/libmm.so
%{_libdir}/calls/plugins/mm/mm.plugin
%{_libdir}/calls/plugins/dummy/dummy.plugin
%{_libdir}/calls/plugins/dummy/libdummy.so

# ofono is retired so we exclude the plugins 4/24/2020
%exclude %{_libdir}/calls/plugins/ofono/libofono.so
%exclude %{_libdir}/calls/plugins/ofono/ofono.plugin

%{_datadir}/applications/sm.puri.Calls.desktop
%{_datadir}/icons/hicolor/scalable/apps/sm.puri.Calls.svg
%{_datadir}/icons/hicolor/symbolic/apps/sm.puri.Calls-symbolic.svg
%{_datadir}/metainfo/sm.puri.Calls.metainfo.xml

%dir %{_datadir}/locale/*
%dir %{_datadir}/locale/*/LC_MESSAGES
%{_datadir}/locale/*/LC_MESSAGES/calls.mo

%doc README.md
%license COPYING

%changelog
* Mon Oct 05 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.8-1
- Updating version 0.1.8

* Sun Jul 26 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.7-1
- Updating version 0.1.7 with patches for openSUSE

* Sat Jun 20 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.4-1
- Updating version 0.1.4.

